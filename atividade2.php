<?php
$CPF = '258.873.544';
$RG = '15.126.687-X';
$CNPJ = '08.126.388/05455‐00';
$nome = 'Carlos Eduardo';
$localizacao = ' R. Paraíba, 1229 ‐ Marília, SP, 17517‐060.';
$cargo = 'Docente';
$empresa = 'Senac';
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Declração de local de trabalho</title>
</head>

<body>

    <h1>DECLARAÇÃO DE LOCAL DE TRABALHO</h1>

    <p>
        Eu, <?= $nome ?>, brasileiro, <?= $cargo ?>, inscrito(a) no CPF sob o nº<?= $CPF ?> e no RG
        nº <?= $RG ?>, declaro para os devidos fins que possuo vínculo empregatício com a empresa
        Senac inscrita no CNPJ sob o nº <?= $CNPJ ?>, localizada à <?= $localizacao ?>
    </p>
    
    <p>
        Por ser expressão da verdade, firmo a presente para efeitos legais.
    </p>
    
    <p>
        Marília – SP, 15 de Setembro de 2022
    </p>

    <br><br>
    
    <p>
        <?= $nome ?>
    </p>
</body>

</html>
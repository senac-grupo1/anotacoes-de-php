<?php 

/*
    #camelCase
    #snak_case

    Tipos de Variaveis

    -string
    -number(integer, float, double)
    -boolean
    -array
    -object
    -null


*/

$curso = 'PHP'; //string
$cargaHoraria = 120; //Inteiro ou integer
$valorCurso = 199.90; //double ou float
$inscrecaoAberta = true; //boolean
$conteudo = ['Variável', 'Função', 'POO' ]; //array

class aluno{};

$aluno = new Aluno(); //object

$frenquencia = null; //null

echo $cargaHoraria;
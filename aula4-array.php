<?php

#Arrray Unidimencional
$listaCompra = ['Arroz', 'Feijão', 'Banana', 'Detergente', 'Sabonete'];

$listaCompra[] = "Ração PET";

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo "<hr>";

var_dump($listaCompra);

echo $listaCompra[0].', '. $listaCompra[2];

echo "<hr>";

foreach($listaCompra as $item){
    echo $item. ", ";

}

echo "<hr>";

#Array Associativo

$funcionario = [
    "nome" => "Carlos Eduardo",
    "cargo" => "MEP",
    "idade" => 17,
    "salario" => 1500.50,
    "ativo" => true,
];
var_dump($funcionario);
echo $funcionario["cargo"];

echo "<hr>";

#Array Multidimencional

$funcionarios = [
    [
        "nome" => "Carlos Eduardo",
        "cargo" => "MEP",
        "idade" => 17,
        "salario" => 1500.50,
        "ativo" => true,
        "cursos" => ["web designer", "web", "JavaScript", "PHP", "Python"]
    ],
    [
        "nome" => "João",
        "cargo" => "MEP",
        "idade" => 45,
        "salario" => 2000,
        "ativo" => false,
        "cursos" => []
    ],
    [
        "nome" => "Lurdinha",
        "cargo" => "MEP",
        "idade" => 50,
        "salario" => 1500.50,
        "ativo" => true,
        "cursos" => ["Photoshop","Ilustrator"]
    ],
];

var_dump($funcionarios);

echo $funcionarios[0]['cursos']["3"];

echo "<br>";
echo "<p>";
echo "Nome: ". $funcionarios[2]['nome'];
echo "<br>";
echo "Cargo: ".$funcionarios[2]['cargo'];
echo "<br>";
echo "Cursos: ".$funcionarios[2]['cursos']["0"];
echo "</p>";

echo "<hr>";

foreach ($funcionarios as $item){

    echo "Nome:".$item["nome"];   
    echo "<br>";
    echo "Cargo:".$item["cargo"]; 
    echo "<br>";
    echo "Idade:".$item["idade"];
    echo "<br>";
    echo "Cursos: ".implode(", ", $item['cursos']);
    echo "<br>";
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>